﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Category
{
    public class CategoryForBrandViewModel
    {
        public int Id { get; set; }

        public string FaTitle { get; set; }
    }
}
