﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DigiKala.Core.ViewModels.Category
{
    public class CreateCategoryViewModel
    {

        [Display(Name = "عنوان فارسی")]
        [Required(ErrorMessage = "لطفا عنوان فارسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان فارسی نباید بیش از 100 کاراکتر باشد")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [Required(ErrorMessage = "لطفا عنوان انگلیسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان انگلیسی نباید بیش از 100 کاراکتر باشد")]

        public string EnTitle { get; set; }


        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".توضیحات نباید بیش از 100 کاراکتر باشد")]
        public string Description { get; set; }


        [Display(Name = "نام عکس")] 
        [Required(ErrorMessage = "لطفا نام عکس را وارد نمایید.")]
        public IFormFile Image { get; set; }
         
        public List<int> ParentList { get; set; }
    }
    public class GetCategoryForAddViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }


    public class EditCategoryViewModel
    {
        public int  Id { get; set; }

        [Display(Name = "عنوان فارسی")]
        [Required(ErrorMessage = "لطفا عنوان فارسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان فارسی نباید بیش از 100 کاراکتر باشد")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [Required(ErrorMessage = "لطفا عنوان انگلیسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان انگلیسی نباید بیش از 100 کاراکتر باشد")]

        public string EnTitle { get; set; }


        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".توضیحات نباید بیش از 100 کاراکتر باشد")]
        public string Description { get; set; }

        public string CurrentImage { get; set; }

        [Display(Name = "نام عکس")]
        [Required(ErrorMessage = "لطفا نام عکس را وارد نمایید.")]
        public IFormFile Image { get; set; }

        public List<int> ParentList { get; set; }
    }

}
