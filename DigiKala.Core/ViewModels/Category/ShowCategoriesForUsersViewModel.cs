﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Category
{
    public class ShowCategoriesForUsersViewModel
    {
        public string FaTitle { get; set; }

       public List<Digikala.DataLayer.Entities.DigikalaCategory.Category> categories { get; set; }
    }
}
