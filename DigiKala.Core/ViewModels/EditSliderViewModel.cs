﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DigiKala.Core.ViewModels
{
    public class EditSliderViewModel
    {
        public int SliderId { get; set; }

        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        [MaxLength(50, ErrorMessage = "{0}نباید بیشتراز {1} باشد.")]
        public string Description { get; set; }


        [Display(Name = "عکس دسکتاپ")] 
        public IFormFile DesktopImg { get; set; }

        [Display(Name = "عکس موبایل")] 
        public IFormFile MobileImg { get; set; }
         
        public string CurrentImageName { get; set; }

        [Display(Name = "ترتیب")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        public int Sort { get; set; }

        [Display(Name = "لینک")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        [MaxLength(150, ErrorMessage = "{0}نباید بیشتراز {1} باشد.")]
        public string Link { get; set; }
    }
}
