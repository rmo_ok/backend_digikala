﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class ProductListViewModel
    {
        [Display(Name = "کد محصول")]
        public int Id { get; set; }

        [Display(Name = "عنوان")]
        public string FaTitle { get; set; }

        [Display(Name = "عکس")] 
        public string Image { get; set; }
    }

    public class AddProductViewModel
    {
        [Display(Name = "عنوان فارسی")]
        [MaxLength(300, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [MaxLength(300, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string EnTitle { get; set; } 

        [Display(Name = "تصویر اصلی")]
        public IFormFile ImgName { get; set; }

        [Display(Name = "دسته‌بندی")]
        public int CategoryId { get; set; }

        [Display(Name = "برند")]
        public int BrandId { get; set; }

        [Display(Name = "انتشار")] 
        public bool IsPublished { get; set; }
    }
}
