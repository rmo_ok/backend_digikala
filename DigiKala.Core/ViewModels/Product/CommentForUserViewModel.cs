﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class CommentForUserViewModel
    {

        public int CommentId { get; set; }

        public string CommentTitle { get; set; }

        public string CommentText { get; set; }

        public string Negative { get; set; }

        public string Positive { get; set; }

        public string UserName { get; set; }

        public int CommentLike { get; set; }

        public int CommentDisLike { get; set; }

        public DateTime CreateDate { get; set; }


    }
}
