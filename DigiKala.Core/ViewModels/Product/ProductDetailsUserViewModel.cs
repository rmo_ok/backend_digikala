﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
   public class ProductDetailsUserViewModel
    {
        public int ProductId { get; set; }

        public string FaTitle { get; set; }

        public string EnTitle { get; set; }

        public string ImgName { get; set; }

        public string CategoryName { get; set; }

        public string BrandName { get; set; }

    }
}
