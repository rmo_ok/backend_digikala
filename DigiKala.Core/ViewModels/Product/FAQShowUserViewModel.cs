﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class FAQShowUserViewModel
    {
        public int QuestionId { get; set; }

        public string QuestionText { get; set; }

        public int AnswerCount { get; set; }

        public DateTime CreateDate { get; set; }

        public string QuestionUser { get; set; }

        public AnswerViewModel  Answer { get; set; }
    }
}
