﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class ProductReviewViewModel
    {
        public string Summary { get; set; }

        public string ShortReview { get; set; }

        public string Reivew { get; set; }

        public string Positive { get; set; }

        public string Negative { get; set; }

        public List<ReviewRatingViewModel> ReviewRatingViewModels { get; set; }
    }
}
