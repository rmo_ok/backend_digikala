﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class ProductPropertyUserViewModel
    {
        public string GroupName { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }
    }
}
