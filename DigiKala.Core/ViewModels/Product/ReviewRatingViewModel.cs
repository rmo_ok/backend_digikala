﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels.Product
{
    public class ReviewRatingViewModel
    {
        public string Title { get; set; }

        public int Value { get; set; } 
    }
}
