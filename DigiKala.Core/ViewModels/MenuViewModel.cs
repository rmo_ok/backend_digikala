﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DigiKala.Core.ViewModels
{
    public class  CreateMenuViewModel
    { 
        [Display(Name = "عنوان منو")]
        [MaxLength(50, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ParentMenuTitle { get; set; }

        [Display(Name = "لینک")]
        [MaxLength(250, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ParentLink { get; set; }

        [Display(Name = "ترتیب")]
        [Range(0,255,ErrorMessage = "{0} باید بین {1} و {2} باشد.")]
        [RegularExpression("^[0-9]+$",ErrorMessage ="لطفا فقط عدد وارد نمایید.")]
        public string ParentSort { get; set; }

        public List<CreateSubMenuViewModel> SubMenuList { get; set; }

    }
    public class CreateSubMenuViewModel
    {
        [Display(Name = "عنوان منو")]
        [MaxLength(50, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string SubMenuTitle { get; set; }

        [Display(Name = "لینک")]
        [MaxLength(250, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string SubMenuLink { get; set; }

        [Display(Name = "ترتیب")]
        public int SubMenuSort { get; set; }

        public IFormFile Image { get; set; }

        public  int Type { get; set; } 
        public bool IsHidden { get; set; }
    }

    public class EditMenuViewModel
    {
        public int ParentMenuId { get; set; }

        [Display(Name = "عنوان منو")]
        [MaxLength(50, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ParentMenuTitle { get; set; }

        [Display(Name = "لینک")]
        [MaxLength(250, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string ParentLink { get; set; }

        [Display(Name = "ترتیب")]
        [Range(0, 255, ErrorMessage = "{0} باید بین {1} و {2} باشد.")] 
        public int ParentSort { get; set; }

        public List<EditSubMenuViewModel> SubMenuList { get; set; }

    }
    public class EditSubMenuViewModel
    {
        public int  SubMenuId { get; set; }

        [Display(Name = "عنوان منو")]
        [MaxLength(50, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string SubMenuTitle { get; set; }

        [Display(Name = "لینک")]
        [MaxLength(250, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string SubMenuLink { get; set; }

        [Display(Name = "ترتیب")]
        public int SubMenuSort { get; set; }

        [Display(Name = "عکس فعلی")] 
        public string CurrentImage { get; set; }

        public IFormFile Image { get; set; }

        public int Type { get; set; }
        public bool IsHidden { get; set; }
    }
}
