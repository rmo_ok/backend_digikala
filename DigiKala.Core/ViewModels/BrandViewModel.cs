﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ViewModels
{
    public class BrandForAddProductViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
