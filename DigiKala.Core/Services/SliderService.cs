﻿using Digikala.DataLayer.Entities;
using Digikala.DataLayer.Entities.Context;
using DigiKala.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class SliderService : ISliderService
    {
        DigiContext _context;

        public SliderService(DigiContext context)
        {
            _context = context;
        }

        public List<Slider> GetSlidersForMin()
        {
            return _context.Sliders.OrderBy(s => s.Sort).Take(6).ToList();
        }
        public int GetSliderCount()
        {
           return _context.Sliders.Count();
        }
        public List<Slider> GetSlidersForAdmin(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.Sliders.Skip(skip).Take(4).ToList();
        }
        public Slider FindSliderById(int id)
        {
           return _context.Find<Slider>(id);
        }

        public bool DeleteSlider(Slider slider)
        {
            _context.Sliders.Remove(slider);

            int result = _context.SaveChanges();

            if (result > 0) 
                return true;

            return false;
        }

        public bool AddSlider(Slider slider)
        {
            _context.Add(slider);

            int result = _context.SaveChanges();

            if (result > 0)
                return true;

            return false;
        }
        public bool UpdateSlider(Slider slider)
        {
            _context.Update(slider);

            int result = _context.SaveChanges();

            if (result > 0)
                return true;

            return false;
        }
    }
}
