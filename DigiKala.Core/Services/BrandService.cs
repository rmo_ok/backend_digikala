﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.DigikalaBrand;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class BrandService : IBrandService
    {
        DigiContext _Context;
        public BrandService(DigiContext Context)
        {
            _Context = Context;
        }
        public Brand GetBrandByName(string brandTitle)
        {
            return _Context.Brands.FirstOrDefault(b => b.EnTitle == brandTitle);
        }

        public List<BrandForAddProductViewModel> GetBrandsForAddProduct()
        {
           return _Context.Brands.Select(b => new BrandForAddProductViewModel {
                Id = b.BrandId,
                Title = b.FaTitle

            }).ToList();
        }
    }
}
