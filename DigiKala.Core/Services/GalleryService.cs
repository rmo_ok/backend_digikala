﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.Product;
using DigiKala.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class GalleryService : IGalleryService
    {
        DigiContext _context;
        public GalleryService(DigiContext context)
        {
            _context = context;
        }

        public List<ProductImage> GetProductImagesForAdmin(int id)
        {
           return _context.ProductImage.Where(pi => pi.ProductId==id).ToList();
        }

        public bool AddProductImage(ProductImage img)
        {
            try
            {
                _context.Add(img);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            } 
        }
    }
}
