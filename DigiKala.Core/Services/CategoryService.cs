﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Category;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class CategoryService : ICategoryService
    {
        DigiContext _context;
        public CategoryService(DigiContext context)
        {
            _context = context;
        }

        public Category GetCategoryByName(string catname)
        {
            return _context.Categories.FirstOrDefault(c => c.EnTitle == catname);
        }

        public List<Category> GetSubCategoryById(int parentid)
        {
            List<Category> categories = (from c in _context.Categories
                                         join s in _context.SubCategories
                                         on c.CategoryId equals s.SubId
                                         where s.ParentId == parentid
                                         select c).ToList();
            return categories;
        }

        public ShowCategoriesForUsersViewModel GetSubCategoryByName(string catname)
        {
            Category category = GetCategoryByName(catname);

            if (category == null)
                return null;
            ShowCategoriesForUsersViewModel showcategory = new ShowCategoriesForUsersViewModel
            {
                FaTitle = category.FaTitle,

                categories = GetSubCategoryById(category.CategoryId)

            };
            return showcategory;
        }

        public List<CategoryForBrandViewModel> GetCategoryByBrandId(int brandid)
        {
            IQueryable<CategoryForBrandViewModel> categories = (from c in _context.Categories
                                                                join b in _context.BrandCategories
                                                                on c.CategoryId equals b.CategoryId
                                                                where b.BrandId == brandid
                                                                select new CategoryForBrandViewModel
                                                                {

                                                                    Id = c.CategoryId,
                                                                    FaTitle = c.FaTitle

                                                                });
            return categories.ToList();
        }

        public List<Category> GetCategoriesForAdmin(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.Categories.Skip(skip).Take(4).ToList();
        }
        public int GetCategoryCount()
        {
            return _context.Categories.Count();
        }
        public bool DeleteCategory(Category category)
        {
            _context.Update(category);

            int res = _context.SaveChanges();

            if (res > 0)
            {
                return true;
            }

            return false;
        }

        public Category FindCategoryById(int id)
        {

            return _context.Find<Category>(id);
        }

        public List<GetCategoryForAddViewModel> GetCategoriesForAdd()
        {
            return _context.Categories.Select(c => new GetCategoryForAddViewModel
            {

                Id = c.CategoryId,

                Title = c.FaTitle

            }).ToList();
        }

        public bool AddCategory(Category category, List<int> parentlist)
        {
            _context.Add(category);
            int res = _context.SaveChanges();

            if (res > 0)
            {
                if (parentlist != null)
                {
                    AddOrUpdateParentCategory(category.CategoryId, parentlist);
                }

                return true;
            }
            return false;
        }

        public void AddOrUpdateParentCategory(int subcatid, List<int> newparentlist)
        {
            List<SubCategory> addlist = new List<SubCategory>();
            List<SubCategory> parentlist = _context.SubCategories.AsNoTracking().Where(s => s.SubId == subcatid).ToList();
            if (parentlist.Count > 0)
            {
                List<SubCategory> removelist = new List<SubCategory>();
                foreach (var item in parentlist)
                {
                    if (newparentlist!=null)
                    {
                        if (newparentlist.Contains(item.ParentId))
                        {
                            newparentlist.Remove(item.ParentId);
                        }
                        else
                        {
                            removelist.Add(new SubCategory {  

                                Id =item.Id, 
                            });
                        }
                    }
                    else
                    {
                        removelist.Add(new SubCategory
                        { 
                            Id = item.Id, 
                        });
                    }
                }

                if (newparentlist != null && newparentlist.Count>0)
                {
                    foreach (var item in newparentlist)
                    {
                        if (!_context.SubCategories.Any(s => s.SubId == subcatid && s.ParentId == item))
                        {
                            addlist.Add(new SubCategory
                            {
                                SubId = subcatid,
                                ParentId = item

                            });
                        }
                    }
                }
                _context.RemoveRange(removelist);
            }
            else
            { 
                if (newparentlist != null)
                {
                    foreach (var item in newparentlist)
                    {
                        if (!_context.SubCategories.Any(s => s.SubId == subcatid && s.ParentId == item))
                        {
                            addlist.Add(new SubCategory
                            {
                                SubId = subcatid,
                                ParentId = item

                            });
                        }
                    }
                   
                }
            }
            _context.AddRange(addlist);
            _context.SaveChanges();
        }

        public List<int> GetSubCategory(int id)
        {
            return _context.SubCategories.Where(s => s.SubId == id).Select(s => s.ParentId).ToList();
        }

        public bool IsExistCategoryTitle(int catId,string entitle)
        {
            return _context.Categories.Any(c => c.CategoryId != catId && c.EnTitle == entitle);
        }

        public bool UpdateCategory(Category category, List<int> parentlist)
        {
            _context.Update(category);

            int result = _context.SaveChanges();

            if (result > 0)
            {
                AddOrUpdateParentCategory(category.CategoryId, parentlist);
                return true;
            }

            return false;
        }
   
        public List<int> GetParentCategory(int id)
        {
            List<int> parenlist = new List<int>();

            List<int> temp = new List<int>();

            List<int> temp2 = _context.SubCategories.Where(s => s.SubId == id).Select(s => s.ParentId).ToList();

            label1: parenlist.AddRange(temp2);

            temp.Clear();

            temp.AddRange(temp2);

            temp2.Clear();

            foreach (var item in temp)
            {

                temp2.AddRange(_context.SubCategories.Where(s => s.SubId == item).Select(s => s.ParentId).ToList());

                if (item.Equals(temp.Last()))
                {
                    goto label1;
                }
            }

            return parenlist.Distinct().ToList();
        }
        
        public bool AddProductCategory(List<ProductCategory> productcategory)
        {
            _context.AddRange(productcategory);

            int res = _context.SaveChanges();

            if (res >0)
            {
                return true;
            }
            return false;
        }
    }
}
