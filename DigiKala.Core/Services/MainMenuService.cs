﻿using Digikala.DataLayer.Entities;
using Digikala.DataLayer.Entities.Context;
using DigiKala.Core.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class MainMenuService : IMainMenuService
    {
        DigiContext _context;
        public MainMenuService(DigiContext context)
        {
            _context = context;
        }

        public int AddParentMenu(MainMenu mainMenu)
        {
            _context.Add(mainMenu);

            int result = _context.SaveChanges();

            return mainMenu.MenuId;
        }

        public bool AddSubMenu(List<MainMenu> submenu)
        {
            _context.AddRange(submenu);

            int res = _context.SaveChanges();

            if (res > 0)
            {
                return true;
            }
            return false;
        }

        public MainMenu GetParentMenu(int id)
        {
            return _context.MainMenus.Where(m => m.ParentId == null && m.MenuId == id).SingleOrDefault();
        }

        public bool DeleteMenu(MainMenu menu)
        {
            bool res = true;
            List<MainMenu> sublist = _context.MainMenus.Where(m => m.ParentId == menu.MenuId).ToList();
              
            if (sublist != null && sublist.Count > 0)
            {
                res = DeleteSubMenu(sublist);
            }
            if (res)
            {
                _context.Remove(menu);
                int result = _context.SaveChanges();

                if (result >0)
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public bool DeleteSubMenu(List<MainMenu> Submenu)
        {
            _context.RemoveRange(Submenu);
            int result = _context.SaveChanges();
            if (result > 0)
            {
                return true;
            }
            return false;
        }
      
        public List<MainMenu> GetMenuListForAdmin(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.MainMenus.Skip(skip).Take(4).Where(m => m.ParentId ==null).ToList();
        }
        public int GetMenuCount()
        {
            return _context.MainMenus.Count();
        }
        public List<MainMenu> GetSubMenuForEdit(int id)
        {
            return _context.MainMenus.AsNoTracking().Where(m => m.ParentId == id).ToList();
        }

        public bool EditParentMenu(MainMenu menu)
        {
            _context.Update(menu);
            int result = _context.SaveChanges();
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public bool UpdateSubMenu(List<MainMenu> menulist)
        {
            _context.UpdateRange(menulist);
            int result = _context.SaveChanges();
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}
