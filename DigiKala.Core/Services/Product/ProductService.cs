﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.Product;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Product;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class ProductService : IProductService
    {
        private DigiContext _context;
        public ProductService(DigiContext context)
        {
            _context = context;
        }

        public ProductDetailsUserViewModel GetProductDetailUser(string EnTitle)
        {
            IQueryable<ProductDetailsUserViewModel> product = (from p in _context.Products
                                                               where p.EnTitle == EnTitle
                                                               join c in _context.Categories on p.CategoryId equals c.CategoryId
                                                               join b in _context.Brands on p.BrandId equals b.BrandId
                                                               select new ProductDetailsUserViewModel
                                                               {
                                                                   ProductId = p.ProductId,
                                                                   EnTitle = p.EnTitle,
                                                                   FaTitle = p.FaTitle,
                                                                   CategoryName = c.FaTitle,
                                                                   BrandName = b.FaTitle,
                                                                   ImgName = p.ImgName

                                                               });
            return product.SingleOrDefault();
        }

        public ProductReviewViewModel GetproductReview(int productid)
        {
            ProductReviewViewModel review = (from r in _context.ProductReviews
                          where r.ProductId == productid
                          select new ProductReviewViewModel
                          {
                              Reivew = r.Riview,
                              ShortReview = r.ShortReview,
                              Summary = r.Summary,
                              Positive = r.Positive,
                              Negative = r.Negative, 
                          }).SingleOrDefault();

            List<ReviewRatingViewModel> rating = new List<ReviewRatingViewModel>();
            rating = (from r in _context.ProductReviewRatings
                      where r.ProductId == productid
                      join attr in _context.RatingAttributes
                      on r.RatingAttributeId equals attr.RatingAttributeId
                      select new ReviewRatingViewModel { 
                      
                          Title = attr.Title,
                          Value = r.Value
                      
                      }).ToList();
            review.ReviewRatingViewModels = rating;
            return review;
        }

        public List<ProductPropertyUserViewModel> GetProperty(int productid)
        {
           List<ProductPropertyUserViewModel> property = (from p in _context.ProductProperties
                                                     join v in _context.PropertyValues
                                                     on p.PropertyValueId equals v.PropertyValueId
                                                     join n in _context.PropertyNames
                                                     on v.PropertyNameId equals n.PropertyNameId
                                                     join g in _context.PropertyGroups
                                                     on n.PropertyGroupId equals g.PropertyGroupId
                                                     where p.ProductId == productid
                                                     select
                      new ProductPropertyUserViewModel
                      {
                          GroupName= g.Title,
                          PropertyName = n.Title,
                          PropertyValue = v.Value

                      }).ToList();

            //List<ProductPropertyUserViewModel> q = _context.ProductProperties
            //.Select(p => new ProductPropertyUserViewModel
            //{
            //    GroupName = p.PropertyValue.PropertyName.PropertyGroup.Title,
            //    PropertyName = p.PropertyValue.PropertyName.Title,
            //    PropertyValue = p.PropertyValue.Value

            //}).ToList();
            return property;
        }
    
        public Tuple<int,List<ProductListViewModel>> GetProductForAdmin(string searchtext, int pagenumber, int take)
        {
            int skip = (pagenumber - 1) * take;
            IQueryable<ProductListViewModel> query= _context.Products.Where(p => EF.Functions.Like(p.EnTitle,"%" + searchtext + "%") || EF.Functions.Like(p.FaTitle, "%" + searchtext + "%")).Select(p => new ProductListViewModel
            {
                Id = p.ProductId,
                FaTitle = p.FaTitle,
                Image = p.ImgName,

            });
            return Tuple.Create(query.Count(), query.Skip(skip).Take(take).ToList());
        }
        public int GetProductCount()
        {
            return _context.Products.Count();
        }
        public int AddProduct(Digikala.DataLayer.Entities.Product.Product prouct)
        {
            _context.Add(prouct);
            _context.SaveChanges();
            return prouct.ProductId;
        }

        public List<ProductListViewModel> GetProductForAdmin()
        {
            throw new NotImplementedException();
        }
    }
}
