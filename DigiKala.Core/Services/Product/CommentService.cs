﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.Product.Comments;
using DigiKala.Core.Services.Interfaces;

using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class CommentService : ICommentService
    {
        DigiContext _context;
        public CommentService(DigiContext context)
        {
            _context = context;
        }

        public Tuple<List<CommentForUserViewModel>, List<ReviewRatingViewModel>, int> GetCommetForUser(int id, int take)
        {

            IQueryable<CommentForUserViewModel> comment = (from c in _context.Comment
                                                           join u in _context.Users on c.UserId equals u.UserId
                                                           where c.ProductId == id
                                                           select new CommentForUserViewModel
                                                           {

                                                               CommentId = c.CommentId,
                                                               CommentTitle = c.CommentTitle,
                                                               CommentText = c.CommentText,
                                                               Positive = c.Positive,
                                                               Negative = c.Negative,
                                                               CommentLike = c.CommentLike,
                                                               CommentDisLike = c.CommentDisLike,
                                                               CreateDate = c.CreateDate,
                                                               UserName = u.FullName
                                                           });
            var count = comment.Count();

            List<ReviewRatingViewModel> rating = (from c in _context.CommentRatings
                                                  join r in _context.RatingAttributes
                                                  on c.RatingAttributeId equals r.RatingAttributeId
                                                  select new ReviewRatingViewModel
                                                  {
                                                      Title = r.Title,
                                                      Value = c.Value
                                                  }).ToList();
            return Tuple.Create(comment.Take(take).ToList(), rating.ToList(), count);
        }

        public List<CommentForUserViewModel> GetCommentsByFilter(int id, int pagenumber, int sort, int take)
        {
            int skip = (pagenumber - 1) * take;
            IQueryable<Comment> comments = _context.Comment.Where(c => c.ProductId == id);
            if (sort == 2)
            {
                comments = comments.OrderByDescending(c => c.CommentLike);
            }
            if (sort == 3)
            {
                comments = comments.OrderByDescending(c => c.CreateDate);
            }

            List<CommentForUserViewModel> commentlist = comments.Select(c => new CommentForUserViewModel
            { 
                CommentId = c.CommentId,
                CommentTitle = c.CommentTitle,
                CommentText = c.CommentText,
                Positive = c.Positive,
                Negative = c.Negative,
                CommentLike = c.CommentLike,
                CommentDisLike = c.CommentDisLike,
                CreateDate = c.CreateDate,
                UserName = c.User.FullName
            }).Skip(skip).Take(take).ToList();
            return commentlist;
        }
    }
}
