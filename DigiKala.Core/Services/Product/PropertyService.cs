﻿using Digikala.DataLayer.Entities.Context;
using Digikala.DataLayer.Entities.Property;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services.Product
{
    public class PropertyService : IPropertyService
    {
        private DigiContext _context;

        public PropertyService(DigiContext context)
        {
            _context = context;
        }

        #region Gorups

        public List<PropertyGroup> GetPropertyGroup(int pagenum)
        {
            int skip = (pagenum - 1) * 4;
            return _context.PropertyGroups.Skip(skip).Take(4).ToList();
        }
        public int GetPropertyGroupCount()
        {
            return _context.PropertyGroups.Count();
        }
        public List<PropertySelectViewModel> GetPropertyGroupForAddNames()
        {
            return _context.PropertyGroups.Select(g => new PropertySelectViewModel
            {
                Id = g.PropertyGroupId,
                Title = g.Title

            }).ToList();
        }
        #endregion

        #region Names
        public bool AddPropertyName(PropertyName name)
        {
            _context.Add(name);
            int res = _context.SaveChanges();
            if (res > 0)
            {
                return true;
            }
            return false;
        }
        //public List<PropertyName> GetPropertyName()
        //{
        //    return _context.PropertyNames.ToList();
        //}
        public List<PropertyName> GetPropertyName(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.PropertyNames.Skip(skip).Take(4).ToList();
        }
        public int GetPropertyNameCount()
        {

            return _context.PropertyNames.Count();
        }

        public PropertyName GetPropertyNameForEdit(int id)
        {
            return _context.Find<PropertyName>(id);
        }

        public bool EditPropertyName(PropertyName name)
        {
            _context.Update(name);
            int res = _context.SaveChanges();
            if (res > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region propertyvalue

        public List<PropertyValue> GetPropertyVale(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.PropertyValues.Skip(skip).Take(4).ToList();
        }

        public int GetPropertyValeCount()
        {
            return _context.PropertyValues.Count();
        }
        public List<PropertySelectViewModel> GetPropertyNameForAddValue()
        {
            return _context.PropertyNames.Select(n => new PropertySelectViewModel
            {
                Id = n.PropertyNameId,
                Title = n.Title
            }).ToList();
        }
        public bool AddPropertyValue(PropertyValue value)
        {
            _context.Add(value);
            int res = _context.SaveChanges();
            if (res > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

    }
}
