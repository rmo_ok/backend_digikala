﻿using Digikala.DataLayer.Entities.Context;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class FAQService : IFAQService
    {
        DigiContext _context;
        public FAQService(DigiContext context)
        {
            _context = context;
        }

        public int FaqCount(int productid)
        {
            return _context.Questions.Where(p => p.ProductId == productid).Count();
        }


        public List<FAQShowUserViewModel> GetFAQ(int productid, int pageid, int sort, int take)
        {
            int skip = (pageid - 1) * take;
            var query = _context.Questions.Where(q => q.ProductId == productid && q.IsConfirm==true);
            switch (sort)
            {
                case 1:
                    query = query.OrderByDescending(p => p.CreatDate);
                    break;

                case 2:
                    query = query.OrderByDescending(p => p.AnswerCount);
                    break;
            }
            var q = (from question in query.Skip(skip).Take(take)
                     join quser in _context.Users on question.UserId equals quser.UserId
                     join a in _context.Answers on question.QuestionId equals a.QuestionId
                     into answers
                     from a in answers.DefaultIfEmpty()
                         //DefaultIfEmpty() baraye tabdil kardan be leftjoin ast.  video_part:212
                     join answeruser in _context.Users on a.UserId equals answeruser.UserId
                      into auser
                     from answeruser in auser.DefaultIfEmpty()
                     select new FAQShowUserViewModel
                     {
                         QuestionId = question.QuestionId,
                         QuestionText = question.QuestionText,
                         CreateDate = question.CreatDate,
                         AnswerCount = question.AnswerCount,
                         QuestionUser = quser.FullName,
                         Answer = new AnswerViewModel
                         {
                             AnswerId = a.AnswerId,
                             AnswerText = a.AnswerText,
                             AnswerDate = (a.CreateDate).ToString(),
                             AnswerUser = answeruser.FullName,
                             Like = a.AnswerLike,
                             DisLike = a.AnswerDisLike
                         }

                     }).ToList();

            
            return q;
        }
       
    }
}
