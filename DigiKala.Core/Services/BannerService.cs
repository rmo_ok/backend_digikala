﻿using Digikala.DataLayer.Entities.Banners;
using Digikala.DataLayer.Entities.Context;
using DigiKala.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiKala.Core.Services
{
    public class BannerService : IBannerService
    {
        DigiContext _context;
        public BannerService(DigiContext context)
        {
            _context =  context;
        }

        public List<Banner> GetBannerForAdmin(int pagenumber)
        {
            int skip = (pagenumber - 1) * 4;
            return _context.Banners.Skip(skip).Take(4).ToList();
        }
        public int GetBannerCount()
        {
           return _context.Banners.Count();
        }
        public bool ChangeActiveBanner(int id)
        {
           Banner banner =  _context.Find<Banner>(id);
           banner.IsActive = banner.IsActive == true ? false : true;
           _context.Update(banner);
            int res = _context.SaveChanges();
            if (res > 0)
            {
                return true;
            }
            return false; 
        }
    }
}
