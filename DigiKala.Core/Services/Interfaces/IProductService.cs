﻿using Digikala.DataLayer.Entities.Product;
using DigiKala.Core.ViewModels.Product;
 
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IProductService
    {
        ProductDetailsUserViewModel GetProductDetailUser(string EnTitle);
        ProductReviewViewModel GetproductReview(int productid);
        List<ProductPropertyUserViewModel> GetProperty(int productid);
        Tuple<int, List<ProductListViewModel>> GetProductForAdmin(string searchtext,int pagenumber,int take);
        int GetProductCount();
        int AddProduct(Digikala.DataLayer.Entities.Product.Product prouct); 
    }
}
