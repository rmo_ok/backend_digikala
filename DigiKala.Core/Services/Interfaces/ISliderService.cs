﻿using Digikala.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
     public interface ISliderService
     {
        List<Slider> GetSlidersForMin();
        List<Slider> GetSlidersForAdmin(int pagenumber);
        Slider FindSliderById(int id);
        bool DeleteSlider(Slider slider);
        bool AddSlider(Slider slider);
        bool UpdateSlider(Slider slider);
        int GetSliderCount();
     }
}
