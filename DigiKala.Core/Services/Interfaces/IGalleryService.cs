﻿
using Digikala.DataLayer.Entities.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IGalleryService
    {
        List<ProductImage> GetProductImagesForAdmin(int id);
        bool AddProductImage(ProductImage img);
    }
}
