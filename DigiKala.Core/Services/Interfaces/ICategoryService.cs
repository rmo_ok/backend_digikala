﻿using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product;
using DigiKala.Core.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface ICategoryService
    {
        Category GetCategoryByName(string catname);
        List<Category> GetSubCategoryById(int parentid);
        ShowCategoriesForUsersViewModel GetSubCategoryByName(string catname);

        List<CategoryForBrandViewModel> GetCategoryByBrandId(int brandid);

        List<Category> GetCategoriesForAdmin(int pagenumber);
        int GetCategoryCount();
        Category FindCategoryById(int id);
        bool DeleteCategory(Category category);
        List<GetCategoryForAddViewModel> GetCategoriesForAdd();
        bool AddCategory(Category category, List<int> parentlist);
        void AddOrUpdateParentCategory(int subcatid, List<int> parentlist);
        List<int> GetSubCategory(int id);
        bool IsExistCategoryTitle(int catId, string entitle);
        public bool UpdateCategory(Category category, List<int> parentlist);
        List<int> GetParentCategory(int id);
        bool AddProductCategory(List<ProductCategory> productcategory);
    }
}
