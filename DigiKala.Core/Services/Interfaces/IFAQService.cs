﻿using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IFAQService
    {
        List<FAQShowUserViewModel> GetFAQ(int productid, int pageid, int sort, int take);
        int FaqCount(int productid);
    }
}
