﻿using Digikala.DataLayer.Entities.Property;
using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IPropertyService
    {
        List<PropertyGroup> GetPropertyGroup(int pagenum);
        int GetPropertyGroupCount();
        List<PropertySelectViewModel> GetPropertyGroupForAddNames();
        bool AddPropertyName(PropertyName name);
        List<PropertyName> GetPropertyName(int pagenumber);
        //List<PropertyName> GetPropertyName();
        int GetPropertyNameCount();
        PropertyName GetPropertyNameForEdit(int id);
        bool EditPropertyName(PropertyName name);
        List<PropertyValue> GetPropertyVale(int pagenumber);
        int GetPropertyValeCount();
        bool AddPropertyValue(PropertyValue value);
        List<PropertySelectViewModel> GetPropertyNameForAddValue();
    }
}
