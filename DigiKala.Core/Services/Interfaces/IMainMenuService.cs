﻿using Digikala.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IMainMenuService
    {
       int AddParentMenu(MainMenu mainMenu);

        bool AddSubMenu(List<MainMenu> submenu);
        List<MainMenu> GetMenuListForAdmin(int pagenumber);
        int GetMenuCount();
        bool DeleteSubMenu(List<MainMenu> Submenu);
        bool DeleteMenu(MainMenu menu);
        MainMenu GetParentMenu(int id);
        List<MainMenu> GetSubMenuForEdit(int id);
        bool EditParentMenu(MainMenu menu);
        bool UpdateSubMenu(List<MainMenu> menulist);
    }
}
