﻿using DigiKala.Core.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface ICommentService
    {
        Tuple<List<CommentForUserViewModel>, List<ReviewRatingViewModel>, int> GetCommetForUser(int productid, int take);
        List<CommentForUserViewModel> GetCommentsByFilter(int id, int pagenumber, int sort , int take);


    }
}
