﻿using Digikala.DataLayer.Entities.DigikalaBrand;
using DigiKala.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IBrandService
    {
        Brand GetBrandByName(string brandTitle);
        List<BrandForAddProductViewModel> GetBrandsForAddProduct();
    }
}
