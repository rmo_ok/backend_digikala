﻿using Digikala.DataLayer.Entities.Banners;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Services.Interfaces
{
    public interface IBannerService
    {
        List<Banner> GetBannerForAdmin(int pagenumber);
        bool ChangeActiveBanner(int id);
        int GetBannerCount();
    }
}
