﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.Security
{
    public static class ImageSecurity
    {
        public static bool  ImageValidator(IFormFile file)
        {
            if(file.Length > 0 && file != null)
            {
                try
                {
                    System.Drawing.Image.FromStream(file.OpenReadStream());
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
    }
}
