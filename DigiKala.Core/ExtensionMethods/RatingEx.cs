﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigiKala.Core.ExtensionMethods
{
    public static class RatingEx
    {
        public static string GetRatingRange(this int value)
        {
            if (value > 75) return "عالی";

            if(value > 50) return "خوب";

            if (value > 25) return "معمولی";

              return "بد";
        }

        public static string[] GetEvaluation(this string value)
        {
            string[] str = value.Split("=");

            return str;
        }
    }
}
