﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
namespace DigiKala.Core.ExtensionMethods
{
    public static class DateTimeEx
    {
        public enum PersianMonth { فروردین = 1, اردیبهشت, خرداد, تیر, مرداد, شهریور, مهر, آبان, آذر, دی, بهمن, اسفند };
        public static string GetMonthPersian(this DateTime dt)
        {
            PersianCalendar pc = new PersianCalendar();

            string month = ((PersianMonth)pc.GetMonth(dt)).ToString();
            return pc.GetDayOfMonth(dt) + " " + month + " " + pc.GetYear(dt);
        }
    }
}
