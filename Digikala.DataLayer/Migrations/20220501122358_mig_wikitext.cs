﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class mig_wikitext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WikiText",
                table: "PropertyValues",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WikiText",
                table: "PropertyNames",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WikiText",
                table: "PropertyGroups",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Color",
                table: "BannerImages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WikiText",
                table: "PropertyValues");

            migrationBuilder.DropColumn(
                name: "WikiText",
                table: "PropertyNames");

            migrationBuilder.DropColumn(
                name: "WikiText",
                table: "PropertyGroups");

            migrationBuilder.DropColumn(
                name: "Color",
                table: "BannerImages");
        }
    }
}
