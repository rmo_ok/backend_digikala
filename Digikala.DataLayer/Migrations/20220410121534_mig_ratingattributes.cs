﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class mig_ratingattributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryRating_RatingTitles_RatingTitleId",
                table: "CategoryRating");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductReviewRatings_RatingTitles_RatingTitleId",
                table: "ProductReviewRatings");

            migrationBuilder.DropTable(
                name: "RatingTitles");

            migrationBuilder.DropIndex(
                name: "IX_ProductReviewRatings_RatingTitleId",
                table: "ProductReviewRatings");

            migrationBuilder.DropIndex(
                name: "IX_CategoryRating_RatingTitleId",
                table: "CategoryRating");

            migrationBuilder.AddColumn<int>(
                name: "RatingAttributeId",
                table: "ProductReviewRatings",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RatingAttributeId",
                table: "CategoryRating",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RatingAttributes",
                columns: table => new
                {
                    RatingAttributeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RatingAttributes", x => x.RatingAttributeId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductReviewRatings_RatingAttributeId",
                table: "ProductReviewRatings",
                column: "RatingAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryRating_RatingAttributeId",
                table: "CategoryRating",
                column: "RatingAttributeId");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryRating_RatingAttributes_RatingAttributeId",
                table: "CategoryRating",
                column: "RatingAttributeId",
                principalTable: "RatingAttributes",
                principalColumn: "RatingAttributeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductReviewRatings_RatingAttributes_RatingAttributeId",
                table: "ProductReviewRatings",
                column: "RatingAttributeId",
                principalTable: "RatingAttributes",
                principalColumn: "RatingAttributeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryRating_RatingAttributes_RatingAttributeId",
                table: "CategoryRating");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductReviewRatings_RatingAttributes_RatingAttributeId",
                table: "ProductReviewRatings");

            migrationBuilder.DropTable(
                name: "RatingAttributes");

            migrationBuilder.DropIndex(
                name: "IX_ProductReviewRatings_RatingAttributeId",
                table: "ProductReviewRatings");

            migrationBuilder.DropIndex(
                name: "IX_CategoryRating_RatingAttributeId",
                table: "CategoryRating");

            migrationBuilder.DropColumn(
                name: "RatingAttributeId",
                table: "ProductReviewRatings");

            migrationBuilder.DropColumn(
                name: "RatingAttributeId",
                table: "CategoryRating");

            migrationBuilder.CreateTable(
                name: "RatingTitles",
                columns: table => new
                {
                    RatingTitleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RatingTitles", x => x.RatingTitleId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductReviewRatings_RatingTitleId",
                table: "ProductReviewRatings",
                column: "RatingTitleId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryRating_RatingTitleId",
                table: "CategoryRating",
                column: "RatingTitleId");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryRating_RatingTitles_RatingTitleId",
                table: "CategoryRating",
                column: "RatingTitleId",
                principalTable: "RatingTitles",
                principalColumn: "RatingTitleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductReviewRatings_RatingTitles_RatingTitleId",
                table: "ProductReviewRatings",
                column: "RatingTitleId",
                principalTable: "RatingTitles",
                principalColumn: "RatingTitleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
