﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class mig_ratingattributesrels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RatingTitleId",
                table: "ProductReviewRatings");

            migrationBuilder.DropColumn(
                name: "RatingTitleId",
                table: "CategoryRating");

            migrationBuilder.AlterColumn<int>(
                name: "RatingAttributeId",
                table: "ProductReviewRatings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RatingAttributeId",
                table: "CategoryRating",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "RatingAttributeId",
                table: "ProductReviewRatings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "RatingTitleId",
                table: "ProductReviewRatings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "RatingAttributeId",
                table: "CategoryRating",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "RatingTitleId",
                table: "CategoryRating",
                nullable: false,
                defaultValue: 0);
        }
    }
}
