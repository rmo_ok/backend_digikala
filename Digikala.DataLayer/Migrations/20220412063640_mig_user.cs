﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class mig_user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_MyProperty_UserId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_NumberOfCommentLikes_MyProperty_UserId",
                table: "NumberOfCommentLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCommentRatings_MyProperty_UserId",
                table: "UserCommentRatings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MyProperty",
                table: "MyProperty");

            migrationBuilder.RenameTable(
                name: "MyProperty",
                newName: "Users");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Users_UserId",
                table: "Comment",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NumberOfCommentLikes_Users_UserId",
                table: "NumberOfCommentLikes",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCommentRatings_Users_UserId",
                table: "UserCommentRatings",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Users_UserId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_NumberOfCommentLikes_Users_UserId",
                table: "NumberOfCommentLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCommentRatings_Users_UserId",
                table: "UserCommentRatings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "MyProperty");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MyProperty",
                table: "MyProperty",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_MyProperty_UserId",
                table: "Comment",
                column: "UserId",
                principalTable: "MyProperty",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NumberOfCommentLikes_MyProperty_UserId",
                table: "NumberOfCommentLikes",
                column: "UserId",
                principalTable: "MyProperty",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCommentRatings_MyProperty_UserId",
                table: "UserCommentRatings",
                column: "UserId",
                principalTable: "MyProperty",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
