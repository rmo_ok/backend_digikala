﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class mig_comment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CommentRatings",
                columns: table => new
                {
                    CommentRatingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<byte>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    RatingAttributeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentRatings", x => x.CommentRatingId);
                    table.ForeignKey(
                        name: "FK_CommentRatings_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommentRatings_RatingAttributes_RatingAttributeId",
                        column: x => x.RatingAttributeId,
                        principalTable: "RatingAttributes",
                        principalColumn: "RatingAttributeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MyProperty",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MyProperty", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    CommentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentTitle = table.Column<string>(maxLength: 100, nullable: false),
                    CommentText = table.Column<string>(maxLength: 3000, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    IsConfirm = table.Column<bool>(maxLength: 100, nullable: false),
                    CommentLike = table.Column<int>(nullable: false),
                    CommentDisLike = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comment_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comment_MyProperty_UserId",
                        column: x => x.UserId,
                        principalTable: "MyProperty",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCommentRatings",
                columns: table => new
                {
                    UserCommentRatingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    RatingAttributeId = table.Column<int>(nullable: false),
                    Value = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCommentRatings", x => x.UserCommentRatingId);
                    table.ForeignKey(
                        name: "FK_UserCommentRatings_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCommentRatings_RatingAttributes_RatingAttributeId",
                        column: x => x.RatingAttributeId,
                        principalTable: "RatingAttributes",
                        principalColumn: "RatingAttributeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCommentRatings_MyProperty_UserId",
                        column: x => x.UserId,
                        principalTable: "MyProperty",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NumberOfCommentLikes",
                columns: table => new
                {
                    NumberOfCommentLikeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<bool>(nullable: false),
                    CommentId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NumberOfCommentLikes", x => x.NumberOfCommentLikeId);
                    table.ForeignKey(
                        name: "FK_NumberOfCommentLikes_Comment_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comment",
                        principalColumn: "CommentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NumberOfCommentLikes_MyProperty_UserId",
                        column: x => x.UserId,
                        principalTable: "MyProperty",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_ProductId",
                table: "Comment",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_UserId",
                table: "Comment",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CommentRatings_ProductId",
                table: "CommentRatings",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_CommentRatings_RatingAttributeId",
                table: "CommentRatings",
                column: "RatingAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_NumberOfCommentLikes_CommentId",
                table: "NumberOfCommentLikes",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_NumberOfCommentLikes_UserId",
                table: "NumberOfCommentLikes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCommentRatings_ProductId",
                table: "UserCommentRatings",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCommentRatings_RatingAttributeId",
                table: "UserCommentRatings",
                column: "RatingAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCommentRatings_UserId",
                table: "UserCommentRatings",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentRatings");

            migrationBuilder.DropTable(
                name: "NumberOfCommentLikes");

            migrationBuilder.DropTable(
                name: "UserCommentRatings");

            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "MyProperty");
        }
    }
}
