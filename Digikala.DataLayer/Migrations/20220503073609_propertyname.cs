﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digikala.DataLayer.Migrations
{
    public partial class propertyname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "PropertyNames",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PropertyNames_CategoryId",
                table: "PropertyNames",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_PropertyNames_Categories_CategoryId",
                table: "PropertyNames",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PropertyNames_Categories_CategoryId",
                table: "PropertyNames");

            migrationBuilder.DropIndex(
                name: "IX_PropertyNames_CategoryId",
                table: "PropertyNames");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "PropertyNames");
        }
    }
}
