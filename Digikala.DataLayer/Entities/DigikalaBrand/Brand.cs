﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.DigikalaBrand
{
    public class Brand
    {

        [Key]
        public int BrandId { get; set; }


        [Display(Name = "عنوان فارسی")]
        [Required(ErrorMessage = "لطفا عنوان فارسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان فارسی نباید بیش از 100 کاراکتر باشد")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [Required(ErrorMessage = "لطفا عنوان انگلیسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان انگلیسی نباید بیش از 100 کاراکتر باشد")]

        public string EnTitle { get; set; }


        [Display(Name = "نام عکس")]
        [MaxLength(50, ErrorMessage = ".نام عکس نباید بیش از 50 کاراکتر باشد")]
        [Required(ErrorMessage = "لطفا نام عکس را وارد نمایید.")]
        public string ImgName { get; set; }


        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".توضیحات نباید بیش از 100 کاراکتر باشد")]
        public string Description { get; set; }

        public List<BrandCategory> BrandCategories { get; set; }

        public List<Product.Product> Products { get; set; }

    }
}
