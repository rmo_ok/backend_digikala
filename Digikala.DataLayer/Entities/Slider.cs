﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities
{
    public class Slider
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "نام عکس")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        [MaxLength(50, ErrorMessage = "{0}نباید بیشتراز {1} باشد.")]
        public string ImageName { get; set; }

        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        [MaxLength(50, ErrorMessage = "{0}نباید بیشتراز {1} باشد.")]
        public string Description { get; set; }

        [Display(Name = "ترتیب")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        public int Sort { get; set; }

        [Display(Name = "لینک")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید.")]
        [MaxLength(150, ErrorMessage = "{0}نباید بیشتراز {1} باشد.")]
        public string Link { get; set; }
    }
}
