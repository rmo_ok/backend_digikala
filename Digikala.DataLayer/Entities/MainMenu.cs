﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Digikala.DataLayer.Entities
{
    public class MainMenu
    {
        [Key]
        public int MenuId { get; set; }

        [Display(Name = "عنوان منو")]
        [MaxLength(50, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string MenuTitle { get; set; }

        [Display(Name = "لینک")]
        [MaxLength(250, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Link { get; set; }

        [Display(Name = "ترتیب")] 
        public int Sort { get; set; }

        [Display(Name = "عکس")]
        [MaxLength(150, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")] 
        public string ImagName { get; set; }

        [Display(Name = "نوع منو")] 
        public byte Type { get; set; } 

        public int? ParentId { get; set; }



        [ForeignKey("ParentId")]
        public List<MainMenu> MainMenus { get; set; }
    }
}
