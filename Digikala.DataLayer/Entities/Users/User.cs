﻿using Digikala.DataLayer.Entities.Product.Comments;
using Digikala.DataLayer.Entities.Product.FAQ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Users
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Display(Name = "نام و نام خانوادگی")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string FullName { get; set; }
         

        public List<Comment> Comments { get; set; }

        public List<NumberOfCommentLike> NumberOfCommentLikes { get; set; }
        public List<UserCommentRating> UserCommentRatings { get; set; }

        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
        public List<AnswerLike> AnswersLikes { get; set; }
    }
}
