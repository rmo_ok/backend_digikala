﻿using Digikala.DataLayer.Entities.Banners;
using Digikala.DataLayer.Entities.DigikalaBrand;
using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product;
using Digikala.DataLayer.Entities.Product.Comments;
using Digikala.DataLayer.Entities.Product.FAQ;
using Digikala.DataLayer.Entities.Property;
using Digikala.DataLayer.Entities.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
namespace Digikala.DataLayer.Entities.Context
{
    public class DigiContext:DbContext
    {
        public DigiContext(DbContextOptions<DigiContext> options):base(options)
        {

        }

        #region table
        public DbSet<Slider> Sliders { get; set; } 
        public DbSet<Brand> Brands { get; set; } 
        public DbSet<BrandCategory> BrandCategories { get; set; }

        public DbSet<MainMenu> MainMenus { get; set; }

        #endregion


        #region Category
        public DbSet<Category> Categories { get; set; } 
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<CategoryRating> CategoryRating { get; set; }
        #endregion


        #region product

        public DbSet<Product.Product> Products { get; set; }
        public DbSet<ProductImage> ProductImage { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<RatingAttribute> RatingAttributes { get; set; }
        public DbSet<ProductReviewRating> ProductReviewRatings { get; set; }
        public DbSet<ProductProperty> ProductProperties { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; } 

        #endregion

        #region Comment
        public DbSet<Comment> Comment { get; set; }
        public DbSet<CommentRating> CommentRatings { get; set; }
        public DbSet<UserCommentRating> UserCommentRatings { get; set; }
        public DbSet<NumberOfCommentLike> NumberOfCommentLikes { get; set; }
        #endregion
        #region
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<AnswerLike> AnswerLikes { get; set; } 
        #endregion
        #region property
        public DbSet<PropertyGroup> PropertyGroups { get; set; }
        public DbSet<PropertyName> PropertyNames { get; set; }
        public DbSet<PropertyValue> PropertyValues { get; set; }
        #endregion

        #region User 
        public DbSet<User> Users { get; set; }

        #endregion


        #region Banner

        public DbSet<Banner> Banners { get; set; }
        public DbSet<BannerImage> BannerImages { get; set; }
        #endregion






        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasQueryFilter(c => !c.IsDeleted);
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            base.OnModelCreating(modelBuilder);
        }
    }
}
