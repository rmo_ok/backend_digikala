﻿using Digikala.DataLayer.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.FAQ
{
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }

        [Display(Name = "متن سوال")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string QuestionText { get; set; }
        public bool IsConfirm { get; set; }
        public int AnswerCount { get; set; }
        [Display(Name = "تاریخ ایجاد")]
        public DateTime CreatDate { get; set; }


        public int UserId { get; set; }
        public User User { get; set; }

        public int ProductId { get; set; } 
        public Product Product { get; set; }

        public List<Answer> Answers { get; set; }
    }
}
