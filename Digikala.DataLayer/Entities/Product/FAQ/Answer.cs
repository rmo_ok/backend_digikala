﻿using Digikala.DataLayer.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.FAQ
{
    public class Answer
    {
        [Key]
        public int AnswerId { get; set; }

        [Display(Name = "متن پاسخ")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string AnswerText { get; set; }
        public int AnswerLike { get; set; }
        public int AnswerDisLike { get; set; }
        public bool IsConfirm { get; set; }

        [Display(Name = "تاریخ ایجاد")]
        public DateTime CreateDate { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public List<AnswerLike> AnswerLikes { get; set; }
    }
}
