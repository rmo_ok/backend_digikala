﻿using Digikala.DataLayer.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.FAQ
{
   public class AnswerLike
    {
        [Key]
        public int AnswerLikeId { get; set; }  
        public bool Type { get; set; }



        public int UserId { get; set; }
        public User User { get; set; }
        public int AnswerId { get; set; }
        public Answer Answer { get; set; }
    }
}
