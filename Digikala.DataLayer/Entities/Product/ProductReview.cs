﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product
{
    public class ProductReview
    {
        [Key]
        public int ProductReviewId { get; set; }

        [Display(Name = "خلاصه")]
        [MaxLength(5000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string Summary { get; set; }


        [Display(Name = "نقد و بررسی کوتاه")]
        [MaxLength(10000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string ShortReview { get; set; }

        [Display(Name = "نقد و بررسی تخصصی")] 
        public string Riview { get; set; }

        [Display(Name = "نقاط قوت")]
        [MaxLength(600, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string Positive { get; set; }

        [Display(Name = "نقات ضعف")]
        [MaxLength(600, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string Negative { get; set; }



        public int ProductId { get; set; }

        public Product Product { get; set; }
    }
}
