﻿using Digikala.DataLayer.Entities.Property;
using System;
using System.Collections.Generic;
using System.Text;

namespace Digikala.DataLayer.Entities.Product
{
    public class ProductProperty
    {
        
   
        public int ProductPropertyId { get; set; }


        public int ProductId { get; set; }
        public Product Product { get; set; }


        public int PropertyValueId { get; set; }
        public PropertyValue PropertyValue { get; set; }

    }
}
