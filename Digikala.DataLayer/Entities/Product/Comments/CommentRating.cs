﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.Comments
{
    public class CommentRating
    {
        [Key]
        public int CommentRatingId { get; set; }

        [Display(Name = "مقدار")]
        public byte Value { get; set; }


        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int RatingAttributeId { get; set; }
        public RatingAttribute RatingAttribute { get; set; }
    }
}
