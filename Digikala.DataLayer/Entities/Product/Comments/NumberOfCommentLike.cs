﻿using Digikala.DataLayer.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.Comments
{
    public class NumberOfCommentLike
    {
        [Key]
        public int NumberOfCommentLikeId { get; set; } 
        public bool Type { get; set; } 



        public int CommentId { get; set; }
        public Comment Comment { get; set; }


        public int UserId { get; set; }
        public User User { get; set; } 
    }
}
