﻿using Digikala.DataLayer.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product.Comments
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        [Display(Name = "عنوان نظر")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string CommentTitle { get; set; }

        [Display(Name = "متن نظر")]
        [MaxLength(3000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string CommentText { get; set; }


        [Display(Name = "نقاط قوت")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string Positive { get; set; }

        [Display(Name = "نقاط ضعف")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string Negative { get; set; }


        [Display(Name = "تاریخ ایجاد")] 
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "تایید شده")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public bool IsConfirm { get; set; }

        [Display(Name = "لایک")]  
        public int CommentLike { get; set; } 

        [Display(Name = "دیس لایک")]  
        public int CommentDisLike { get; set; }  


        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public List<NumberOfCommentLike> NumberOfCommentLikes { get; set; }
    }
}
