﻿using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product.Comments;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product
{
    public class RatingAttribute
    {
        [Key]
        public int RatingAttributeId { get; set; }

        [Display(Name = "نام")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        public List<CategoryRating> CategoryRatings { get; set; }
        public List<ProductReviewRating> ProductReviewRatings { get; set; }
        public List<UserCommentRating> UserCommentRatings { get; set; }
        public List<CommentRating> CommentRatings { get; set; }

    }
}
