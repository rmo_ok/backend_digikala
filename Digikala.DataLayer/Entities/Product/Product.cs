﻿using Digikala.DataLayer.Entities.DigikalaBrand;
using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product.Comments;
using Digikala.DataLayer.Entities.Product.FAQ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Product
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Display(Name = "عنوان فارسی")]
        [MaxLength(300,ErrorMessage ="مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage ="لطفا {0} را وارد نمایید")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [MaxLength(300, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string EnTitle { get; set; }

        [Display(Name = "تاریخ ایجاد")] 
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "تاریخ بروز رسانی")] 
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = " عکس اصلی")] 
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string ImgName { get; set; }

        public bool IsPublished { get; set; }





        public int CategoryId { get; set; } 

        public int BrandId { get; set; } 
        public Brand Brands { get; set; } 
        public Category Categories { get; set; }
        public List<ProductImage> ProductImages { get; set; }

        public List<ProductReview> ProductReviews { get; set; }
        public List<ProductReviewRating> ProductReviewRatings { get; set; }

        public List<ProductProperty> ProductProperties { get; set; }

        public List<Comment> Comments { get; set; }

        public List<UserCommentRating> UserCommentRatings { get; set; }
        public List<CommentRating> CommentRatings { get; set; }

        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }

        List<ProductCategory> ProductCategories { get; set; }
    }
}
