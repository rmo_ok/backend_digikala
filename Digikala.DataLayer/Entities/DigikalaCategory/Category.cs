﻿using Digikala.DataLayer.Entities.DigikalaBrand;
using Digikala.DataLayer.Entities.Product;
using Digikala.DataLayer.Entities.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.DigikalaCategory
{
    public class Category
    {

        [Key]
        public int CategoryId { get; set; }


        [Display(Name = "عنوان فارسی")]
        [Required(ErrorMessage = "لطفا عنوان فارسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان فارسی نباید بیش از 100 کاراکتر باشد")]
        public string FaTitle { get; set; }

        [Display(Name = "عنوان انگلیسی")]
        [Required(ErrorMessage = "لطفا عنوان انگلیسی را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".عنوان انگلیسی نباید بیش از 100 کاراکتر باشد")]

        public string EnTitle { get; set; }


        [Display(Name = "نام عکس")] 
        [MaxLength(50, ErrorMessage = ".نام عکس نباید بیش از 50 کاراکتر باشد")]
        [Required(ErrorMessage = "لطفا نام عکس را وارد نمایید.")]
        public string ImgName { get; set; }

        public bool IsDeleted { get; set; }

        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید.")]
        [MaxLength(100, ErrorMessage = ".توضیحات نباید بیش از 100 کاراکتر باشد")]
        public string Description { get; set; }

        public List<SubCategory> ParentCategory { get; set; }

        public List<SubCategory> SubCategory { get; set; }

        public List<BrandCategory> BrandCategories { get; set; }

        public List<Product.Product> Products { get; set; }
    
        public List<CategoryRating> CategoryRatings { get; set; }
       
        List<ProductCategory> ProductCategories { get; set; }
        List<PropertyName> PropertyNames { get; set; } 
        
    }
}
