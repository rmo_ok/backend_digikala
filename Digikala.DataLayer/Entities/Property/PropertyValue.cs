﻿using Digikala.DataLayer.Entities.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Property
{
    public class PropertyValue
    {
        [Key]
        public int PropertyValueId { get; set; }


        [Display(Name = "مقدار")]
        [MaxLength(200, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Value { get; set; }

        [Display(Name = "متن راهنما")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string WikiText { get; set; }



        public int PropertyNameId { get; set; }
        public PropertyName PropertyName { get; set; }

        public List<ProductProperty> ProductProperties { get; set; }
    }
}
