﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Property
{
    public class PropertyName
    {
        [Key]
        public int PropertyNameId { get; set; }

        [Display(Name = "عنوان")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        [Display(Name = "ترتیب")]
        public int Priority { get; set; }


        [Display(Name = "استفاده در جستجو")]
        public bool UseInSearchPage { get; set; }

        [Display(Name = "متن راهنما")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string WikiText { get; set; }



        [Display(Name = "گروه خصوصیات")]
        public int PropertyGroupId { get; set; }

        public PropertyGroup PropertyGroup { get; set; }

        public List<PropertyValue> PropertyValues { get; set; }

        [Display(Name = "دسته‌بندی")]
        public int CategoryId { get; set; }
        public DigikalaCategory.Category Category { get; set; }

    }
}
