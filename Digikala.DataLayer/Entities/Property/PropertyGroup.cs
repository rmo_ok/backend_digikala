﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Digikala.DataLayer.Entities.Property
{
    public class PropertyGroup
    {
        [Key]
        public int PropertyGroupId { get; set; }

        [Display(Name = "عنوان")]
        [MaxLength(100, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        [Required(ErrorMessage = "لطفا {0} را وارد نمایید")]
        public string Title { get; set; }

        [Display(Name = "ترتیب")]
        public int Priority { get; set; }


        [Display(Name = "متن راهنما")]
        [MaxLength(1000, ErrorMessage = "مقدار{0} نباید بیش از {1} باشد.")]
        public string WikiText { get; set; }

        public List<PropertyName> PropertyNames { get; set; }
    }
}
