using Digikala.DataLayer.Entities.Context;
using DigiKala.Core.Services;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.Services.Product;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<DigiContext>(option => option.UseSqlServer(Configuration.GetConnectionString("DigiKala")));
            services.AddTransient<ISliderService, SliderService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IFAQService, FAQService>();
            services.AddTransient<IMainMenuService, MainMenuService>();
            services.AddTransient<IBannerService, BannerService>();
            services.AddTransient<IPropertyService, PropertyService>();
            services.AddTransient<IGalleryService,  GalleryService>();
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStatusCodePagesWithReExecute("/error/NotExist");
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                 
                endpoints.MapControllerRoute(

                    name: "areas",
                    pattern: "{area=exists}/{Controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("defualt", "{Controller=Home}/{action=Index}/{id?}");
            }); 
        }
    }
}
