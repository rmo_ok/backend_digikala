﻿using DigiKala.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.ViewComponents
{
    public class SliderComponent : ViewComponent
    {
        ISliderService _sliderService;
        public SliderComponent(ISliderService sliderService)
        {
            _sliderService = sliderService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult(View("MainSlider", _sliderService.GetSlidersForMin()));
        }
    }
}
