﻿using DigiKala.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.ViewComponents.Product
{
    public class ProductProperyComponent : ViewComponent
    {
        IProductService _productService;
        public ProductProperyComponent(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<IViewComponentResult> InvokeAsync(int productid)
        {
            return await Task.FromResult(View("ProductProperty", _productService.GetProperty(productid)));
        }
    }
}
