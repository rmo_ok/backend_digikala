﻿using DigiKala.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.ViewComponents
{
    public class ShowCategoryForBrandComponent : ViewComponent
    {
        ICategoryService _CategoryService;
        public ShowCategoryForBrandComponent(ICategoryService CategoryService)
        {
            _CategoryService = CategoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            return await Task.FromResult(View("ShowCategoryForBrand", _CategoryService.GetCategoryByBrandId(id)));
        }
    }
}
