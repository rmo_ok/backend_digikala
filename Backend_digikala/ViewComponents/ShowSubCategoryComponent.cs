﻿using DigiKala.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.ViewComponents
{
    public class ShowSubCategoryComponent : ViewComponent
    {
        ICategoryService _CategoryService;
        public ShowSubCategoryComponent(ICategoryService CategoryService)
        {
            _CategoryService = CategoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            return await Task.FromResult(View("ShowSubCategory", _CategoryService.GetSubCategoryById(id)));
        }
    }
}
