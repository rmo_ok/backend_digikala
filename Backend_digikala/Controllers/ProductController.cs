﻿using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Product;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.Controllers
{
    public class ProductController : Controller
    {
        IProductService _productService;
        ICommentService _commentService;
        IFAQService _faq;
        public ProductController(IProductService productService, ICommentService commentService , IFAQService faq)
        {
            _productService = productService;
            _commentService = commentService;
            _faq = faq;
        }

        [Route("Product/{Entitle}")]
        public IActionResult ProductDetail(string Entitle)
        {
            ProductDetailsUserViewModel product = _productService.GetProductDetailUser(Entitle);
            _faq.GetFAQ(1, 1, 1, 5);
            if (product == null)
                return NotFound();

            return View(product);
        }

        [HttpPost]
        public IActionResult CommentTab(int id, string fatitle)
        {
            ViewBag.fatitle = fatitle;
            ViewBag.idproduct = id;
            Tuple<List<CommentForUserViewModel>, List<ReviewRatingViewModel>, int> comment = _commentService.GetCommetForUser(id, 4);
            ViewBag.commentcount = comment.Item3;
            ViewBag.Commentpagenumber = 1;
            ViewBag.CommetnSort = 1;
            return View(comment);
        }

        [HttpPost]
        public IActionResult CommentList(int id, int pagenumber, int sort, int commentcount)
        {
            ViewBag.idproduct = id;
            ViewBag.commentcount = commentcount; 
            ViewBag.Commentpagenumber = pagenumber;
            ViewBag.CommetnSort = sort;
            return View(_commentService.GetCommentsByFilter(id, pagenumber, sort, 4));
        }

        [HttpPost]
        public IActionResult FAQ(int id, int pagenumber, int sort, int faqcount)
         {
            ViewBag.idproduct = id;
            ViewBag.faqcount = faqcount == 0 ? _faq.FaqCount(id) : faqcount;
            ViewBag.faqpagenumber = pagenumber;
            ViewBag.faqSort = sort;
            return View(_faq.GetFAQ(id, pagenumber, sort, 4));
        }
    }
}
