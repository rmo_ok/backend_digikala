﻿using DigiKala.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Digikala.DataLayer.Entities.DigikalaBrand;
namespace Backend_digikala.Controllers
{
    public class HomeController : Controller
    {
        ICategoryService _categoryService;
        IBrandService _brandservice;
        public HomeController(ICategoryService  categoryService, IBrandService  brandservice)
        {
            _categoryService = categoryService;
            _brandservice = brandservice;
        }

        public IActionResult Index()
        {
            return View();
        }


        [Route("main/{catname}")]
        public IActionResult Category( string catname)
        {
            var categories = _categoryService.GetSubCategoryByName(catname);

            if (categories==null || categories.categories.Count <= 0)
            
                return NotFound();

            return View(categories);
            
        }

        [Route("Brand/{brandtitle}")]
        public IActionResult Brand(string brandTitle)
        {
           Brand brand = _brandservice.GetBrandByName(brandTitle);

            if(brand !=null)
            return View(brand);

            return NotFound();
        }
    }
}
