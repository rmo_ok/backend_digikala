﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.Controllers
{
    
    public class ErrorController : Controller
    {
         
        public IActionResult NotExist()
        {
            return View();
        }
    }
}
