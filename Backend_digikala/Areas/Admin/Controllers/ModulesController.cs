﻿using Digikala.DataLayer.Entities;
using DigiKala.Core.ExtensionMethods;
using DigiKala.Core.Security;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ModulesController : Controller
    {
        IMainMenuService _mainmenuservice;
        public ModulesController(IMainMenuService mainmenuservice)
        {
            _mainmenuservice = mainmenuservice;
        }
        #region
        public IActionResult CreateMenu() => View();

        [HttpPost]
        public IActionResult CreateMenu(CreateMenuViewModel mainmenu)
        {
            if (!ModelState.IsValid)
            {
                return View(mainmenu);
            }
            MainMenu parentmenu = new MainMenu
            {
                MenuTitle = mainmenu.ParentMenuTitle,
                Link = mainmenu.ParentLink,
                Sort = int.Parse(mainmenu.ParentSort),
            };
            int parentid = _mainmenuservice.AddParentMenu(parentmenu);

            if (parentid <= 0)
            {
                return View(mainmenu);
            }

            if (mainmenu.SubMenuList != null && mainmenu.SubMenuList.Count > 0)
            {
                //List<CreateSubMenuViewModel> hiddenlist = mainmenu.SubMenuList.Where(s => s.IsHidden == true).ToList();
                mainmenu.SubMenuList = mainmenu.SubMenuList.Where(s => s.IsHidden == false).ToList();

                List<MainMenu> Sublist = new List<MainMenu>();
                foreach (var item in mainmenu.SubMenuList)
                {
                    string imgname = "";

                    if (item.Image != null)
                    {
                        if (ImageSecurity.ImageValidator(item.Image))
                        {

                            imgname = item.Image.SaveImage("", "wwwroot/Images/Menu");
                        }
                        else
                        {
                            ModelState.AddModelError(String.Empty, "لطفا یک فایل درست انتخاب نمایید.");
                            return View(mainmenu);
                        }
                    }

                    Sublist.Add(new MainMenu
                    {

                        Link = item.SubMenuLink,
                        MenuTitle = item.SubMenuTitle,
                        Sort = item.SubMenuSort,
                        Type = (byte)item.Type,
                        ImagName = imgname,
                        ParentId = parentid,
                    });
                }

                var res = _mainmenuservice.AddSubMenu(Sublist);
                TempData["res"] = res ? "success" : "faild";
                return RedirectToAction("MenuList");
            }

            TempData["res"] = "success";
            return RedirectToAction("MenuList");
        }

        public IActionResult MenuList(int id=1)
        {
            ViewBag.pagecount = _mainmenuservice.GetMenuCount();
            ViewBag.pagenumber = id;
            return View(_mainmenuservice.GetMenuListForAdmin(id));
        }

        [HttpPost]
        public IActionResult DeleteMenu(int id)
        {
            MainMenu menu = _mainmenuservice.GetParentMenu(id);
            if (menu == null)
            {
                TempData["res"] =  "faild";
                return RedirectToAction("MenuList");
            }

            bool res = _mainmenuservice.DeleteMenu(menu);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction("MenuList"); 
        }

        public IActionResult EditMenu(int id)
        {
            MainMenu menu = _mainmenuservice.GetParentMenu(id);
            if (menu == null)
            {
                TempData["res"] = "faild";
                return RedirectToAction("MenuList");
            }
            List<MainMenu> submenu = _mainmenuservice.GetSubMenuForEdit(id);

            List<EditSubMenuViewModel> submenuedit = new List<EditSubMenuViewModel>();
            foreach (var item in submenu)
            {
                submenuedit.Add(new EditSubMenuViewModel { 
                
                    SubMenuTitle=item.MenuTitle,
                    SubMenuLink=item.Link,
                    SubMenuSort = item.Sort,
                    CurrentImage = item.ImagName,
                    Type=item.Type 
                });
            }
            EditMenuViewModel edit = new EditMenuViewModel
            {
                ParentMenuId = menu.MenuId,
                ParentMenuTitle = menu.MenuTitle,
                ParentLink = menu.Link,
                ParentSort = menu.Sort,
                SubMenuList = submenuedit
            };

            return View(edit);
        }
      
        [HttpPost]
        public IActionResult EditMenu(EditMenuViewModel edit)
        {
            if (!ModelState.IsValid)
            {
                return View(edit); 
            }

            int menuid = int.Parse(TempData["ParentMenuId"].ToString());
       
            //for showing  
            MainMenu parentmenu = new MainMenu
            {
                MenuId = menuid,
                MenuTitle = edit.ParentMenuTitle,
                Link = edit.ParentLink,
                Sort = edit.ParentSort, 
            };

            if (!_mainmenuservice.EditParentMenu(parentmenu))
            {
                return View(edit);
            }
            List<MainMenu> oldsubmenu = _mainmenuservice.GetSubMenuForEdit(menuid);
             
            for (int i = 0; i < oldsubmenu.Count; i++)
            {
                edit.SubMenuList[i].SubMenuId = oldsubmenu[i].MenuId;
                edit.SubMenuList[i].CurrentImage = oldsubmenu[i].ImagName; 
            }

            #region delete submenus that are deleted

            //baraye hazfe submenu haye hiddenlist ke az noe EditSubMenuViewModel ast bayad az methode deleteSubmeu
            //estefade konim ke vorodi on za jense mainmenu ast pas bayad hiddenlist ra be mainmenu tabdil konim
            //baraye in kar yek list newlist ra ejad kardim

            List<MainMenu> newlist = new List<MainMenu>();
            List<EditSubMenuViewModel> hiddenlist = edit.SubMenuList.Where(s => s.IsHidden == true).ToList();
            hiddenlist = hiddenlist.Where(s => s.SubMenuId > 0).ToList();
            if (hiddenlist != null && hiddenlist.Count > 0)
            {
                foreach (var item in hiddenlist)
                {
                    if (String.IsNullOrEmpty(item.CurrentImage))
                    {
                        item.CurrentImage.DeleteImage("wwwroot/Images/Menu");
                    }

                    newlist.Add(new MainMenu { 

                        MenuId = item.SubMenuId 
                    });
                }
                if (!_mainmenuservice.DeleteSubMenu(newlist))
                {
                    return View(edit);
                }
            }
            #endregion

            #region addnewsubmenu
            edit.SubMenuList = edit.SubMenuList.Where(s => s.IsHidden == false).ToList();
            List<EditSubMenuViewModel> templist = edit.SubMenuList.Where(s => s.SubMenuId <=0).ToList();
            if (templist !=null && templist.Count > 0)
            {
                newlist.Clear();

                foreach (var item in templist)
                {
                    string imgname = "";

                    if (item.Image != null)
                    {
                        if (ImageSecurity.ImageValidator(item.Image))
                        {

                            imgname = item.Image.SaveImage("", "wwwroot/Images/Menu");
                        }
                        else
                        {
                            ModelState.AddModelError(String.Empty, "لطفا یک فایل درست انتخاب نمایید.");
                            return View(edit);
                        }
                    }

                    newlist.Add(new MainMenu
                    {

                        Link = item.SubMenuLink,
                        MenuTitle = item.SubMenuTitle,
                        Sort = item.SubMenuSort,
                        Type = (byte)item.Type,
                        ImagName = imgname,
                        ParentId = menuid,
                    });
                }

                if (!_mainmenuservice.AddSubMenu(newlist))
                    return View(edit);
            }
            #endregion

            #region updateSubmenu
            templist.Clear();
            templist = edit.SubMenuList.Where(s => s.SubMenuId > 0).ToList();
            if (templist != null && templist.Count > 0)
            {
                newlist.Clear();
                foreach (var item in templist)
                {
                    string imgname = item.CurrentImage;
                    if (item.Image !=null)
                    {
                        if (ImageSecurity.ImageValidator(item.Image))
                        {
                            item.CurrentImage.DeleteImage("wwwroot/Images/Menu");
                            imgname = item.Image.SaveImage("", "wwwroot/Images/Menu");
                        }
                        else
                        {
                            ModelState.AddModelError(String.Empty, "لطفا یک فایل درست انتخاب نمایید.");
                            return View(edit);
                        }
                    }

                    newlist.Add(new MainMenu
                    { 
                        MenuId=item.SubMenuId,
                        Link = item.SubMenuLink,
                        MenuTitle = item.SubMenuTitle,
                        Sort = item.SubMenuSort,
                        Type = (byte)item.Type,
                        ImagName = imgname,
                        ParentId = menuid,
                    });
                }
                if (!_mainmenuservice.UpdateSubMenu(newlist))
                {
                    return View(edit);
                }
            }
            #endregion
            TempData["res"] = "success" ;
            return RedirectToAction("MenuList");
        }
        #endregion
    }
}
