﻿using Digikala.DataLayer.Entities;
using DigiKala.Core.ExtensionMethods;
using DigiKala.Core.Security;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BannerController : Controller
    {
        ISliderService _sliderService;
        IBannerService _bannerservice;
       
        public BannerController(ISliderService sliderService, IBannerService bannerservice)
        {
            _sliderService = sliderService;
            _bannerservice = bannerservice;
        }



        #region slider
        public IActionResult SliderList(int id=1)
        {
            ViewBag.pagecount = _sliderService.GetSliderCount();
            ViewBag.pagenumber = id;
            return View(_sliderService.GetSlidersForAdmin(id));
        }

        [HttpPost]
        public IActionResult DeleteSlider(int id)
        {
           Slider slider= _sliderService.FindSliderById(id);

            if (slider ==null)
            {
                TempData["res"] = "faild";
                return RedirectToAction(nameof(SliderList));
            }
            
                bool res = _sliderService.DeleteSlider(slider);
                if (res)
                {
                    
                    slider.ImageName.DeleteImage("wwwroot/Images/slider");

                    slider.ImageName.DeleteImage("wwwroot/Images/slider/mobile_slider");
                }

                TempData["res"] =  res ? "success" : "faild";
                return RedirectToAction(nameof(SliderList));
        }

        public IActionResult CreatSlider()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreatSlider(CreateSliderViewModel slide)
        {
            if (!ModelState.IsValid)
            {
                return View(slide);
            }
            string filename = "";
            if (ImageSecurity.ImageValidator(slide.DesktopImg))
            {
                filename = slide.DesktopImg.SaveImage("", "wwwroot/Images/slider");
            }
            else
            {
                ModelState.AddModelError("DesktopImg", "لطفا یک فایل درست انتخاب نمایید.");
                return View(slide);
            }

            if (ImageSecurity.ImageValidator(slide.MobileImg))
            {
                  slide.MobileImg.SaveImage(filename, "wwwroot/Images/slider/mobile_slider");
            }
            else
            {
                ModelState.AddModelError("MobileImg", "لطفا یک فایل درست انتخاب نمایید.");
                return View(slide);
            }

            Slider sl = new Slider()
            {
                ImageName = filename,
                Description = slide.Description,
                Link = slide.Link,
                Sort = slide.Sort
            };
            bool res = _sliderService.AddSlider(sl);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(SliderList));
            
        }

        public IActionResult EditSlider(int id)
        {
            Slider slider = _sliderService.FindSliderById(id);

            if (slider==null)
            {
                TempData["res"] = "faild";
                return RedirectToAction(nameof(SliderList));
            }
            EditSliderViewModel edslivm = new EditSliderViewModel()
            {
                Description = slider.Description,
                Link = slider.Link,
                Sort = slider.Sort,
                SliderId = slider.Id,
                CurrentImageName=slider.ImageName
            };
            return View(edslivm);
        }

        [HttpPost]
        public IActionResult EditSlider(EditSliderViewModel slide)
        {
            if (!ModelState.IsValid)
            {
                return View(slide);
            }
            string  filename = TempData["imgname"].ToString();

            if (slide.DesktopImg!=null)
            {
                if (ImageSecurity.ImageValidator(slide.DesktopImg))
                {
                    filename.DeleteImage("wwwroot/Images/slider");
                    slide.DesktopImg.SaveImage(filename, "wwwroot/Images/slider");
                }
                else
                {
                    ModelState.AddModelError("DesktopImg", "لطفا یک فایل درست انتخاب نمایید.");
                    return View(slide);
                }
            }

            if (slide.MobileImg!=null)
            {
                if (ImageSecurity.ImageValidator(slide.MobileImg))
                {
                    filename.DeleteImage("wwwroot/Images/slider/mobile_slider");
                    slide.MobileImg.SaveImage(filename, "wwwroot/Images/slider/mobile_slider");
                }
                else
                {
                    ModelState.AddModelError("MobileImg", "لطفا یک فایل درست انتخاب نمایید.");
                    return View(slide);
                }
            }
            Slider sl = new Slider() {
                Id = int.Parse(TempData["sliderid"].ToString()),
                Description =slide.Description,
                ImageName =filename,
                Sort =slide.Sort,
                Link =slide.Link 
            };
            bool res = _sliderService.UpdateSlider(sl);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(SliderList));
        }
        #endregion

        #region Banner

        public IActionResult BannerList(int id=1)
        {
            ViewBag.pagecount = _bannerservice.GetBannerCount();
            ViewBag.pagenumber = id;
            return View(_bannerservice.GetBannerForAdmin(id));
        }

        public IActionResult ChangeActive(int id)
        {
            TempData["res"] = _bannerservice.ChangeActiveBanner(id) ? "success" : "faild";
            return RedirectToAction(nameof(BannerList));
        }
        #endregion

    }
}
