﻿using Digikala.DataLayer.Entities.DigikalaCategory;
using Digikala.DataLayer.Entities.Product;
using Digikala.DataLayer.Entities.Property;
using DigiKala.Core.ExtensionMethods;
using DigiKala.Core.Security;
using DigiKala.Core.Services.Interfaces;
using DigiKala.Core.ViewModels.Category;
using DigiKala.Core.ViewModels.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_digikala.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CatalogController : Controller
    {
        ICategoryService _categoryService;
        IProductService _productService;
        IBrandService _brandService;
        IPropertyService _propertyService;
        IGalleryService _Galleryservice;
        public CatalogController(ICategoryService categoryService, IProductService productService,
                                 IBrandService brandService, IPropertyService propertyService,
                                 IGalleryService Galleryservice)
        {
            _categoryService = categoryService;
            _productService = productService;
            _brandService = brandService;
            _propertyService = propertyService;
            _Galleryservice = Galleryservice;
        }

        #region category
        public IActionResult CategoryList(int id=1)
        {
            ViewBag.pagecount = _categoryService.GetCategoryCount();
            ViewBag.pagenumber = id;
            return View(_categoryService.GetCategoriesForAdmin(id));
        }

        public IActionResult DeleteCategory(int id)
        {
            Category category = _categoryService.FindCategoryById(id);

            if (category == null)
            {
                TempData["res"] = "failed";
                return RedirectToAction(nameof(CategoryList));
            }
            category.IsDeleted = true;
            bool res = _categoryService.DeleteCategory(category);

            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(CategoryList));
        }

        public IActionResult CreateCategory()
        {
            ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();

            return View();
        }

        [HttpPost]
        public IActionResult CreateCategory(CreateCategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                return View(category);
            }
            if (_categoryService.IsExistCategoryTitle(0, category.EnTitle))
            {
                ModelState.AddModelError("EnTitle", "این نام تکراری است.");
                ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                return View(category);
            }
            string imgname = "";

            if (category.Image != null)
            {
                if (ImageSecurity.ImageValidator(category.Image))
                {

                    imgname = category.Image.SaveImage("", "wwwroot/Images/Category");
                }
                else
                {
                    ModelState.AddModelError("Image", "لطفا یک فایل درست انتخاب نمایید.");
                    return View(category);
                }
            }

            Category cat = new Category
            {
                Description = category.Description,
                FaTitle = category.FaTitle,
                EnTitle = category.EnTitle,
                ImgName = imgname
            };

            bool res = _categoryService.AddCategory(cat, category.ParentList);

            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(CategoryList));
        }
        public IActionResult EditCategory(int id)
        {
            Category category = _categoryService.FindCategoryById(id);

            if (category == null)
            {
                TempData["res"] = "failed";
                return RedirectToAction(nameof(CategoryList));
            }

            EditCategoryViewModel cat = new EditCategoryViewModel
            {
                Id = category.CategoryId,
                FaTitle = category.FaTitle,
                EnTitle = category.EnTitle,
                Description = category.Description,
                CurrentImage = category.ImgName,

            };
            ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
            ViewBag.ParentList = _categoryService.GetSubCategory(id);
            return View(cat);
        }
        [HttpPost]
        public IActionResult EditCategory(EditCategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                category.CurrentImage = TempData["Currentimg"].ToString();
                ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                ViewBag.ParentList = category.ParentList;

                return View(category);
            }

            if (_categoryService.IsExistCategoryTitle(int.Parse(TempData["id"].ToString()), category.EnTitle))
            {
                ModelState.AddModelError("EnTitle", "این نام تکراری است.");
                category.CurrentImage = TempData["Currentimg"].ToString();
                ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                ViewBag.ParentList = category.ParentList;

                return View(category);
            }

            string imgname = TempData["Currentimg"].ToString();

            if (category.Image != null)
            {
                if (ImageSecurity.ImageValidator(category.Image))
                {
                    imgname.DeleteImage("wwwroot/Images/Category");
                    imgname = category.Image.SaveImage(imgname, "wwwroot/Images/Category");
                }
                else
                {
                    ModelState.AddModelError("Image", "لطفا یک فایل درست انتخاب نمایید.");
                    category.CurrentImage = TempData["Currentimg"].ToString();
                    ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                    ViewBag.ParentList = category.ParentList;

                    return View(category);
                }
            }

            Category cat = new Category()
            {
                CategoryId = int.Parse(TempData["id"].ToString()),
                Description = category.Description,
                FaTitle = category.FaTitle,
                EnTitle = category.EnTitle,
                ImgName = imgname

            };

            bool res = _categoryService.UpdateCategory(cat, category.ParentList);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(CategoryList));
        }
        #endregion

        #region  product
        public IActionResult ProductListContainer()
        {
           
            
            return View("~/Areas/Admin/Views/Catalog/Product/ProductListContainer.cshtml");

        }
        public IActionResult ProductList(string searchtext="",int pagenumber=1)
        {
            var content = _productService.GetProductForAdmin(searchtext, pagenumber, 2);
            ViewBag.count = content.Item1;
            ViewBag.searchtext = searchtext;
            ViewBag.pagenumber = pagenumber;
            return View("~/Areas/Admin/Views/Catalog/Product/ProductList.cshtml", content.Item2);
        }

        public IActionResult CreateProduct()
        {
            ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
            ViewBag.Brandlist = _brandService.GetBrandsForAddProduct();
            return View("~/Areas/Admin/Views/Catalog/Product/CreateProduct.cshtml");
        }

        [HttpPost]
        public IActionResult CreateProduct(AddProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                ViewBag.Brandlist = _brandService.GetBrandsForAddProduct();
                return View("~/Areas/Admin/Views/Catalog/Product/CreateProduct.cshtml", product);
            }
            string imgnaem = "";
            if (product.ImgName != null)
            {
                if (ImageSecurity.ImageValidator(product.ImgName))
                {
                    imgnaem = product.ImgName.SaveImage(imgnaem, "wwwroot/Images/Product");
                }
                else
                {
                    ModelState.AddModelError("ImgName", "لطفا یک فایل درست انتخاب نمایید.");
                    ViewBag.Categorylist = _categoryService.GetCategoriesForAdd();
                    ViewBag.Brandlist = _brandService.GetBrandsForAddProduct();

                    return View("~/Areas/Admin/Views/Catalog/Product/CreateProduct.cshtml",product);
                }
            }

            Product p = new Product
            {
                FaTitle = product.FaTitle,
                EnTitle = product.EnTitle,
                ImgName = imgnaem,
                CategoryId = product.CategoryId,
                BrandId = product.BrandId,
                IsPublished = product.IsPublished,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };

            int productid = _productService.AddProduct(p);
            List<int> parentlist = _categoryService.GetParentCategory(product.CategoryId);
            List<ProductCategory> productcategory = new List<ProductCategory>();
            
            foreach (var item in parentlist)
            {
                productcategory.Add(new ProductCategory
                {
                    CategoryId = item,
                    ProductId = productid
                });
            }
             
            bool res = _categoryService.AddProductCategory(productcategory);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(ProductList));
             
        }
        #endregion

        #region property

        public IActionResult PropertyGroupList(int id=1)
        {
            ViewBag.pagecount = _propertyService.GetPropertyGroupCount();
            ViewBag.pagenumber = id;
            return View("~/Areas/Admin/Views/Catalog/Property/PropertyGroupList.cshtml",_propertyService.GetPropertyGroup(id));
        }
     
        public IActionResult PropertyNameList(int id=1)
        {
            ViewBag.pagecount = _propertyService.GetPropertyNameCount();
            ViewBag.pagenumber = id;
            return View("~/Areas/Admin/Views/Catalog/Property/PropertyNameList.cshtml", _propertyService.GetPropertyName(id));
        }
        
        public IActionResult CreatePropertyName()
        {
            ViewBag.Groups = _propertyService.GetPropertyGroupForAddNames();
            ViewBag.Category = _categoryService.GetCategoriesForAdd();
            return View("~/Areas/Admin/Views/Catalog/Property/CreatePropertyName.cshtml");
        }
        
        [HttpPost]
        public IActionResult CreatePropertyName(PropertyName names)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Groups = _propertyService.GetPropertyGroupForAddNames();
                ViewBag.Category = _categoryService.GetCategoriesForAdd();
                return View("~/Areas/Admin/Views/Catalog/Property/CreatePropertyName.cshtml",names);
            }
  
            bool res = _propertyService.AddPropertyName(names);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(PropertyNameList));
        }

        public IActionResult EditPropertyName(int id)
        {
            PropertyName name = _propertyService.GetPropertyNameForEdit(id);
            if (name==null)
            { 
                TempData["res"] =  "faild";
                return RedirectToAction(nameof(PropertyNameList));
            }
            ViewBag.Groups = _propertyService.GetPropertyGroupForAddNames();
            ViewBag.Category = _categoryService.GetCategoriesForAdd();
            return View("~/Areas/Admin/Views/Catalog/Property/EditPropertyName.cshtml", name);
        }

        [HttpPost]
        public IActionResult EditPropertyName(PropertyName names)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Groups = _propertyService.GetPropertyGroupForAddNames();
                ViewBag.Category = _categoryService.GetCategoriesForAdd();
                return View("~/Areas/Admin/Views/Catalog/Property/EditPropertyName.cshtml", names);
            }
            names.PropertyNameId = int.Parse(TempData["id"].ToString());
            bool res = _propertyService.EditPropertyName(names);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(PropertyNameList));
        }
        public IActionResult PropertyValueList(int id=1)
        {
            ViewBag.pagcount   = _propertyService.GetPropertyValeCount();
            ViewBag.pagenumber = id;
            return View("~/Areas/Admin/Views/Catalog/Property/PropertyValueList.cshtml", _propertyService.GetPropertyVale(id));
        }

        public IActionResult CreatePropertyValue()
        {
            ViewBag.names = _propertyService.GetPropertyNameForAddValue();
            return View("~/Areas/Admin/Views/Catalog/Property/CreatePropertyValue.cshtml");
        }
        [HttpPost]
        public IActionResult CreatePropertyValue(PropertyValue valuename)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.names = _propertyService.GetPropertyNameForAddValue(); 
                return View("~/Areas/Admin/Views/Catalog/Property/CreatePropertyValue.cshtml", valuename); 
            }
            bool res = _propertyService.AddPropertyValue(valuename);
            TempData["res"] = res ? "success" : "faild";
            return RedirectToAction(nameof(PropertyValueList));
        }

        /////you have to create edit value and delete action for practice

        #endregion

        #region Gallery
        public IActionResult GalleryList(int id)
        {
            ViewBag.productid = id;
            return View(_Galleryservice.GetProductImagesForAdmin(id));
        }

        public IActionResult CreateProductImage(IFormFile imagename)
        {
            string Imagename = "";
            int id= int.Parse(TempData["Productimage"].ToString());
            if (imagename != null)
            {
                if (ImageSecurity.ImageValidator(imagename))
                {
                    Imagename = imagename.SaveImage("", "wwwroot/Images/Product/gallery");
                    string oldpath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Images/Product/gallery", Imagename);
                    string newpath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Images/Product/gallery/thubms", Imagename);
                    oldpath.Image_resize(newpath, 150, 150);
                    ProductImage productimage = new ProductImage
                    {
                        ImgName = Imagename,
                        ProductId=id
                    };
                    _Galleryservice.AddProductImage(productimage);
                }
                else
                { 
                    return RedirectToAction(nameof(GalleryList), new { id = id });
               
                }
            }
            return RedirectToAction(nameof(GalleryList),new { id =id });
        }

        /// deleting image is a homework
        #endregion
    }
}
